#!/usr/bin/env python3

def main():
    print('This is Main')
    for i in inclusive_range(0, 25, 1):
        print(i, end = ' ')
    print()

    #range_clone()
    range_clone(5)
    range_clone(20, 25)
    range_clone(0, 25, 5)

    print("after range_clone()'s")

def inclusive_range(start, stop, step):
    i = start
    while i <= stop:
        # yield is returned but each time the function
        # is run, execution continues after the yield
        yield i
        i += step

def range_clone(*args):
    numargs = len(args)
    if numargs < 1: raise TypeError('requires at least one argument')
    elif numargs == 1:
        stop = args[0]
        start = 0
        step = 1
    elif numargs == 2:
        (start, stop) = args
        step = 1
    elif numargs == 3:
        (start, stop, step) = args
    else: raise TypeError('range_clone expected at most 3 arguments, got {}'.format(numargs))

    for i in inclusive_range(start, stop, step):
        print(i, end = ' ')
    print()


if __name__ == '__main__':
    main()

