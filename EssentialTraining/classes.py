#!/usr/bin/env python3

class Animal():

    def talk(self): print('I have something to say')
    def walk(self): print('Hey, I''m walking here')
    def clothes(self): print('I have nice clothes')

class Duck(Animal):
    # constructor method
    def __init__(self, value, color = 'white'):
        print('Duck constructed')
        # underscore denotes local attribute
        self._v = value
        self._color = color

    # functions that are attributes of a class are 'methods'
    def quack(self):
        print('QUAAAACK!')

    def walk(self):
        super().walk()
        print('Walks like a duck')

    def set_color(self, color):
        self._color = color

    def get_color(self):
        return self._color

class Penguin():
    def __init__(self, **kwargs):
        print('Penguin contructed')
        self.variables = kwargs

    def set_color(self, color):
        self.variables['color'] = color

    def get_color(self):
        return self.variables.get('color', None)

    def set_variable(self, k, v):
        self.variables[k] = v

    def get_variable(self, k):
        return self.variables.get(k, None)

class Dog(Animal):
    pass

class Cat(Animal):
    def bark(self):
        print('Woof!')

    def fur(self):
        print('The dog has brown fur')

    def walk(self):
        print('I am a cat and I refuse to walk')

def main():
    donald = Duck(42)
    print('donald is a: ', donald)
    donald.quack()
    donald.walk()
    print(donald.get_color())
    donald.set_color('red')
    print(donald.get_color())

    tux = Penguin(color = 'black')
    print(tux.get_color())
    tux.set_variable('beak', 'yellow')
    tux.set_variable('color', 'orange')
    print(tux.get_variable('beak'))
    print(tux.get_variable('color'))

    fido = Dog()
    fido.walk()

    mittens = Cat()
    mittens.bark()

    for o in (fido, mittens):
        o.walk()

if __name__ == '__main__':
    main()
