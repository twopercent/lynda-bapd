#!/usr/bin/env python3


def b(n): print('{:08b}'.format(n))

def bits():
    print('### Bits ###')
    b(5)

    x, y = 0x55, 0xaa

    b(x)
    b(y)

    print('x or y, x and y, x xor 0, x xor 0xff')
    b(x | y)
    b(x & y)
    b(x ^ 0)
    b(x ^ 0xff)

    print('shift operator')
    b(x << 4)
    b(x >> 4)

    print('ones complement')
    b(~ x)

def slicefunc():
    mylist = []
    mylist = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    print(mylist[0])
    print(mylist[0:5])

    nextlist = []
    nextlist[:] = range(100)
    print(nextlist)
    print(nextlist[0:51:5])

    for i in nextlist[0:26:5]:
        print(i)

    nextlist[0:26:5] = ('no', 'no', 'no', 'no', 'yes', 'yes')

    print(nextlist[0:51:5])

def precedence():

    print(5 * 25 + 14 / 2)
    print(5 * ( 25 + 14 ) / 2)


bits()
slicefunc()
precedence()
