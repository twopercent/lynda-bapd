#!/usr/bin/env python3

class inclusive_range:
    def __init__(self, *args):
        numargs = len(args)
        if numargs < 1: raise TypeError('requires at least one argument')
        elif numargs == 1:
            self.stop = args[0]
            self.start = 0
            self.step = 1
        elif numargs == 2:
            (self.start, self.stop) = args
            self.step = 1
        elif numargs == 3:
            (self.start, self.stop, self.step) = args
        else: raise TypeError('range_clone expected at most 3 arguments, got {}'.format(numargs))

    def __iter__(self):
        i = self.start
        while i <= self.stop:
            yield i
            i += self.step


def main():
    o = inclusive_range(0, 25, 2)
    # the inclusive_range object was called as an iterable (for i in ...)
    # so the __iter__ method is used
    for i in o: print(i, end=' ')
    print()



if __name__ == '__main__':
    main()

