#!/usr/bin/env python3

def main():
    infile = open('lines.txt', 'r')
    outfile = open('new.txt', 'w')

    #for line in infile:
    for line in infile.readlines():
        print(line, file = outfile, end = '')

    print('Done')

if __name__ == '__main__': main()
