#!/usr/bin/env python3

import sys

def main():
    print('Python version {}.{}.{}'.format(*sys.version_info))
    print('sys.platform')
    print()

    import os
    print(os.name)
    print(os.getenv('PATH'))
    print(os.getcwd())
    print(os.urandom(25))
    print()

    import urllib.request
    #page = urllib.request.urlopen('http://neic.coop/')
    #for line in page: print(str(line, encoding = 'utf-8'), end = '')
    print()

    import random
    print(random.randint(1,1000))
    x = list(range(25))
    print(x)
    random.shuffle(x)
    print(x)
    print()

    import datetime
    now = datetime.datetime.now()
    print(now)
    print(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond)
    print()

if __name__ == '__main__': main()
