#!/usr/bin/env python3

def main():
    print('Executing Main')
    testfunc2(15)
    testfunc2(15, 11, 12, 31)
    testfunc3(one = 1, six = 6, seven = 7)
    print(testfunc4())

def testfunc(number, another = 43, onemore = None): # default values
    print('This is a test function', number, another, onemore)

def testfunc2(*args):
    print('Type of args', type(args))
    for n in args: print(n, end=' ')
    print()

def testfunc3(**kwargs):
    print('Type of kwargsi (named arguments)', type(kwargs))

    #don't need to know keys of dict
    for k in kwargs: print(k, kwargs[k])

    # if you know keys, you can ues them
    print(kwargs['six'], kwargs['seven'])

def testfunc4():
    return 'This is test function four'

def stubfunc():
    pass

if __name__ == '__main__':
    main()
