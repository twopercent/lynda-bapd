#!/usr/bin/env python3

def main():
    s = 'this is a string'

# continue
    for c in s:
        if c == 's': continue
        print(c, end='')

    print()

# break
    for e in s:
        if e == 's': break
        print(e, end='')

    print()

# else
    for f in s:
        print(f, end='')
    else:
        print('else')

    print()

if __name__ == '__main__':
    main()
