#!/usr/bin/env python3

def main():
    num = 42
    print(type(num), num)

    num = 42.0
    print(type(num), num)

    num = int(42.3)
    print(num)
    num = float(42)
    print(num)
    print()

def strings():
    print('#### Strings ####')
    s = 'this is a \nstring'
    print(s)
    s = r'this is a raw \nstring'
    print(s)
    n = 42
    s = 'This is a {} string'.format(n)
    print(s)
    s = '''\
this is a string
line after line
of text and more text
'''
    print(s)
    print()

def variables():
    print('#### Variables ####')
    x = (1, 2, 3) #tuple immutable
    print(type(x), x)
    x = [1, 2, 3] #list mutable
    print(id(x))
    x.append(5)
    x.insert(2, 7)
    print(type(x), x)
    print(id(x))
    x = 'string'
    print(type(x), x[2:4])
    print()

def dictionary():
    print('#### Dictionary ####')
    d = {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5}
    for k in sorted(d.keys()):
        print(k, d[k])

    print()

    e = dict(
        one = 1, two = 2, three = 2, four = 4, five = 5
    )

    e['seven'] = 7

    for k in (e.keys()):
        print(k, e[k])

def ids():
    print('#### Ids ####')
    print()
    print('variables are immutable')
    x = 42
    print(type(x), id(x))
    y = 42
    print(type(y), id(y))

    print('x == y ', x == y) # compares values
    print('x is y ', x is y) # compares id's

    print()
    print('dicts are mutable, we could change the value of x')
    x = dict( x = 42) 
    print(type(x), id(x), x)
    

    y = dict( x = 42)
    print(type(y), id(y), y)

    print('x == y ', x == y) # compares values
    print('x is y ', x is y) # compares id's

    x['x'] == 43
    print('changing the value of a key in x the dict object is still the same object')
    print(type(x), id(x))

def tf():
    print('#### True False ####')
    print()
    a, b = 0, 1
    print(a == b)
    print(a > b)
    a = True
    b = True
    print(type(a), id(a))
    print(type(b), id(b))

if __name__ == '__main__': main()
#strings()
#variables()
#dictionary()
#ids()
tf()
