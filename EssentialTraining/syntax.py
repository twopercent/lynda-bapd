#!/usr/bin/env python3

class Egg:
    def __init__(self, kind = 'fried'):
        self.kind = kind

    def whatKind(self):
        return self.kind

def main():
    a = 1
    print(a)
    print(type(a))

    a = '1'
    print(a)
    print(type(a))

    a, b = 1, '1'
    print(a, b)

    a, b = 1, 2
    if a < b:
        print('A is less than B')
    elif a > b:
        print('A is greater than B')
    else:
        print('A is equal to B')

# conditional expression
    s = 'less than' if a < b else 'not less than'
    print(s)
    print('A is {} B'.format(s))

    print('This is the syntax.py file')

    egg()

    print()

    func(1)
    func()
    func(5)

    myEgg = Egg()
    myEgg2 = Egg('scrambled')

    print(myEgg.whatKind())
    print(myEgg2.kind)

def egg():
    print('egg')

def func(a=7):
    for i in range(a, 10):
        print(i, end=' ')
    print()


if __name__ == '__main__': main()
