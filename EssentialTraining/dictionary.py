#!/usr/bin/env python3

d = { 'one': 1, 'two': 2, 'three': 3 }
e = dict( one = 1, two = 2, three = 3 )
f = dict( four = 4, five = 5, six = 6, **e)

print('for k in f: print(k)')
for k in f: print(k)
print()

print('for k, v, in f.items(): print(k, v)')
for k, v in f.items(): print(k, v)
print()

# Will error if there is no 'three'
print(f['three'])
# Will return None if there is not 'seven'
print(f.get('seven'))
print(f.get('seven', 'not found'))
