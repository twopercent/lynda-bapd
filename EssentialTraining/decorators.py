#!/usr/bin/env python3

class Duck:
    def __init__(self, **kwargs):
        self.properties = kwargs

    def quack(self):
        print('Quack')

    def walk(self):
        print('I am walking don''t you see?')

    def get_properties(self):
        return self.properties

    def get_property(self, key):
        return self.properties.get(key, None)

    def set_properties(self, k, v):
        self.properties[k] = v

    @property
    def color(self):
        return self.properties.get('color', None)

    @color.setter
    def color(self, c):
        self.properties['color'] = c

    @color.deleter
    def color(self):
        del self.properties['color']

def main():
    # use 'set_property'
    donald = Duck(color = 'blue')
    print(donald.get_property('color'))

    # use decorator methods (@property)
    donald.color = 'mauve'
    print(donald.color)

if __name__ == '__main__':
    main()
