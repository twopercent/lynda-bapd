#!/usr/bin/env python3

def main1():
    a, b = 4, 42

    print('This is {} and this is {}'.format(a, b))
    print('This string {1} switches the variables {0} and adds more {0}'.format(a, b))

    students = dict(student1 = 'steve', student2 = 'molly')
    print('This is {student1} and this is {student2}'.format(**students))

def main2():
    s = 'This is a string of words'
    print(s.split())
    print(s.split('i'))

    words = s.split()
    print(words)

    print('re-assemble...')
    for w in words:
        print(w, end=' ')
    print()

if __name__ == '__main__':
    main2()
