#!/usr/bin/env python3

def main():
    a, b = 0, 1
    if a < b:
        v = 'this is true'
    else:
        v = 'this is not true'
    print(v)

    c, d = 0, 1
    w = 'this is true' if a < b else 'this is not true'
    print(w)

if __name__ == '__main__': main()
