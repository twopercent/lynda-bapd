#!/usr/bin/env python3

def main():
    try:
        for line in readfile('xlines.txxt'):
            print(line.strip())
    except IOError as e:
        print('cannot read file:', e)
    except ValueError as e:
        print('bad filename', e)


def readfile(filename):
    if filename.endswith('.txt'):
        fh = open(filename)
        return fh.readlines()
    else:
        raise ValueError('Filename must end with .txt')

print('after badness')


if __name__ == '__main__':
    main()
