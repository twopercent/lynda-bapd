#!/usr/bin/env python3

import re

def main():
    print('Executing Main...')
    #resub()
    #research()
    #rematch()
    reuse()

def resub():
    print("### Search ###")
    fh = open('raven.txt')
    for line in fh:
        if re.search('(Len|Neverm)ore', line):
            print(line, end='')
    fh.close()

def research():
    print()
    print('### Sub ###')
    fh = open('raven.txt')
    for line in fh:
        print(re.sub('(Len|Neverm)ore', '###', line))
    fh.close()

def rematch():
    print()
    print('### Re Match ###')
    fh = open('raven.txt')
    for line in fh:
        match = re.search('(Len|Neverm)ore', line)
        if match:
            print(line.replace(match.group(), '###'), end='')
    fh.close()

def reuse():
    print()
    print('### Re Use ###')
    fh = open('raven.txt')
    pattern = re.compile('(Len|Neverm)ore', re.IGNORECASE)
    for line in fh:
        if re.search(pattern, line):
            print(line, end='')
    fh.close()

if __name__ == '__main__':
    main()
