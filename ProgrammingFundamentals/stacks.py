#!/usr/bin/env python3

stack = list()

stack.append('bill1')
stack.append('bill2')

print(stack.pop())

stack.append('bill3')
stack.append('bill4')


print(stack.pop())
print(stack.pop())
print(stack.pop())
