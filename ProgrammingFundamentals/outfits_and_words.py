#!/usr/bin/env python3

closet = ['shirt', 'hat', 'pants', 'jacket', 'socks']
print('id(closet) ', id(closet))

closet.remove('hat')
print('removed hat')
print('id(closet) ', id(closet))

words = "You're wearing that?!"
print(words)
print('id(words) ', id(words))

words = words + " because you look great!"
print(words)
print('id(words) ', id(words))


