#!/usr/bin/env python3

""" A 3-Dimenional Valet Service """

# 2D lists of lists

lot_2d = [['Toyota', 'Audi', 'BMW'],
          ['Lexus', 'Jeep'],
          ['Honda', 'Kia', 'Mazda']]

lot_3d = [[['Tesla', 'Fiat', 'BMW'],
           ['Honda', 'Jeep'],
           ['Saab', 'Kia', 'Ford']],
          [['Subaru', 'Nissan'],
           ['Volvo']],
          [['Mazda', 'Chevy'],
           [],
           ['Volkswagen']]]


print(lot_2d)
print(lot_3d)

print()

print(lot_2d[2])
print(lot_2d[2][1])

print()

print(lot_3d[0])
print(lot_3d[0][2])
print(lot_3d[0][2][1])

print()

## print each floor of garage
for car in lot_3d:
    print(car)

## print each car in the garage
for floor in lot_3d:
    for row in floor:
        for car in row:
            print(car)
