#!/usr/bin/env python3

row = ['Ford', 'Audi', 'BMW', 'Lexus']
print(row)

row.append('Mercedes')
print(row)
print(row[4])

row[2]
print(row)

row.append('Honda')
print('row: ', row)
print('row[4]: ', row[4])

row.insert(0, 'kia')

print('insert kia at 0')
print('row[4]: ', row[4])

print('row.inxex("Mercedes"): ', row.index('Mercedes'))

print(row)
print('row.pop(5)')
row.pop(5)

print(row)
print('row.remove("Lexus")')
row.remove('Lexus')
print(row)
