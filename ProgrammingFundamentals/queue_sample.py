#!/usr/bin/env python3

import queue

q = queue.Queue()
print(q.empty())

q.put('bag1')
print(q.empty())

q.put('bag2')
q.put('bag3')

print(q.get())
print(q.get())
print(q.get())
print(q.empty())

#print(q.get())
#print(q.get(False))


newq = queue.Queue(2)
print(newq.empty())
newq.put('bag1')

print(newq.full())
newq.put('bag2')

print(newq.full())
print(q.put_nowait('bag3'))
