#!/usr/bin/env python3

my_tuple = ('a', 'b', 'c', 1, 2, 3)
print(my_tuple)
print(my_tuple[2])

## error:
#my_tuple[2] = 'd'


