#!/usr/bin/env python3

""" A Garage Full of Classy Vehicles """

class Vehicle:

    def __init__(self, color, manuf):
        self.color = color
        self.manuf = manuf
        self.gas = 4 # full

    def drive(self):
        if self.gas > 0:
            self.gas -= 1
            print('The {} {} goes VROOM!'.format(self.color, self.manuf))
        else:
            prnit('The {} {} sputters out of gas.'.format(self.color, self.manuf))

class Car(Vehicle):
    def radio(self):
        print('Rockin Tunes')

    def window(self):
        print('Ahhh... fresh air!')

class Motorcycle(Vehicle):
    def helmet(self):
        print('Nice and safe!')

class ECar(Car):
    def drive(self):
        print('The {} {} goes shhhhhh'.format(self.color, self.manuf))

def main():
    civic = Car('blue', 'honda')
    beemer = Motorcycle('blue', 'BMW')
    soul_ev = ECar('green', 'kia')

    civic.drive()
    print('civic gas = ', civic.gas)
    beemer.helmet()

    soul_ev.drive()

if __name__ == '__main__':
    main()
