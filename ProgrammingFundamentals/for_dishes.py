#!/usr/bin/env python3

""" Loading the Dishwasher """

sink = ['bowl', 'plate', 'cup']

for dish in sink:
    print('Putting {} in the dishwasher'.format(dish))

## To modify a list using a for loop, create a copy

print(sink)

for dish in list(sink):  #second copy we are iteratnig on
    print('Putting {} in the dishwasher'.format(dish))
    sink.remove(dish)   #removing items from original list

print(sink) 
