#!/usr/bin/env python3

""" Creating and Combining Sets of Friends """

college = set(['Bill', 'Katy', 'Verne', 'Dillon',
               'Bruce', 'Olivia', 'Richard', 'Jim'])

coworker = set(['Aaron', 'Bill', 'Brandon', 'David',
                'Frank', 'Connie', 'Kyle', 'Olivia'])

family = set(['Garry', 'Landon', 'Larry', 'Mark',
              'Olivia', 'Katy', 'Rae', 'Tom'])

local = set(['Olivia', 'Landon', 'Larry', 'Dillon',
             'Connie', 'Richard'])

munchkin = set(['Chris', 'Ian', 'Larry'])

print(college)
print(len(college))

friends = college.union(coworker, family)

print('len friends: ', len(friends))
print('friends: ', friends)

local_friends = friends.intersection(local)
print(local_friends)

not_munchkins = local_friends.difference(munchkin)
print(not_munchkins)

invite = local.symmetric_difference(munchkin)
print(invite)

