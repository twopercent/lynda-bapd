#!/usr/bin/env python3

""" Polling for Pizza to Cure My Hunger """

import time

hungry = True

while(hungry):

    front_door = open('front_door.txt', 'r')
    print(front_door)

    if 'Pizza Guy' in front_door:
        print("Pizza's here!")
        hungry = False
    else:
        print('Not yet...')

    print('Closing the front door.')
    front_door.close()

    time.sleep(1)
