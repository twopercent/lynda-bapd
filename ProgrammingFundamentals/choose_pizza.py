#!/usr/bin/env python3

""" Ordering A Pizza That Verne Can Eat """

diet_restrictions = set(['meat', 'cheese'])

if 'meat' and 'cheese' in diet_restrictions:
    print('Get a vegan pizza.')

elif 'meat' in diet_restrictions:
    print('Get a cheese pizza.')

else:
    print('Get something else.')


pizza_special = {'Sunday'    : 'spinach',
                 'Monday'    : 'mushroom',
                 'Tuesday'   : 'pepperoni',
                 'Wednesday' : 'veggie',
                 'Thursday'  : 'bbq chicken',
                 'Friday'    : 'saueage',
                 'Saturday'  : 'Hawaiian'}

def order(day):
    pizza = pizza_special[day]
    print('Order the {} pizza.'.format(pizza))

order('Saturday')
