#!/usr/bin/env python3

class shirt:

    def __init__(self):
        self.clean = True

    def make_dirty(self):
        self.clean = False

    def make_clean(self):
        self.clean = True

def main():
    red = shirt()    
    crimson = red

    print(id(red), id(crimson))
    print(red.clean, crimson.clean)

    red.make_dirty()

    print(red.clean, crimson.clean)

    crimson = shirt()
    print('red is crimson: ', red == crimson)
    
if __name__ == "__main__":
    main()
