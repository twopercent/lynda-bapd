#!/usr/bin/env python3

""" The Blueprints for Jeans """

class jeans:

    def __init__(self, waist, length, color):
        self.waist = waist
        self.length = length
        self.color = color
        self.wearing = False

    def put_on(self):
        print('Putting on {}x{} {} jeans'.format(self.waist, self.length, self.color))
        self.wearing = True
    
    def take_off(self):
        print('Taking off {}x{} {} jeans'.format(self.waist, self.length, self.color))
        self.wearing = False

def main():
    jeans1 = jeans(36, 34, 'blue')

    print('Type: ', type(jeans1))
    print('Dir: ', dir(jeans1))

    print('Wearing? ->', jeans1.wearing)

if __name__ == "__main__":
    main()
