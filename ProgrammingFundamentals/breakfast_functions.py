#!/usr/bin/env python3

### A Functional Breakfast ###

cheese = 'cheddar'


def mix_and_cook():
    print('Mixing the ingredients')
    print('Greasing the frying pan')
    print('Pouring the mixture into a frying pan')
    print('Cooking the first side')
    print('Flipping it!')
    print('Cooking the other side')


def make_omelette():
    global cheese
    cheese = 'swiss'
    mix_and_cook()
    omelette = 'a tasty {} omelette'.format(cheese)
    return omelette

def make_pancake():
    mix_and_cook()
    pancake = 'a delicious {} pancake'.format(cheese)
    return pancake

def fancy_omelette(*ingredients):
    mix_and_cook()
    omelette = 'a fancy omelette with {} ingredients'.format(len(ingredients))
    return omelette

omelette1 = make_omelette()
pancake1 = make_pancake()
fancy_omelette = fancy_omelette('bacon', 'tomato', 'cheese', 'green pepper')

print(omelette1)
print(pancake1)
#print(fancy_omelette)
