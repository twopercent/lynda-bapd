#!/usr/bin/env python3

""" Adding and Removing Friends from Sets """

invite = set(['Nestor', 'Amanda', 'Olivia'])

print('Verne' in invite)
invite.add('Verne')

print(invite)
invite.add('Verne')
print(invite)

invite.remove('Nestor')
print(invite)

print(invite.pop())
print(invite.pop())
print(invite.pop())
