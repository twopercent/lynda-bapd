#!/usr/bin/env python3

""" Overloading a Circuit Breaker """

class CircuitBreaker:

    def __init__(self, max_amps):
        self.capacity = max_amps
        self.load = 0

    def connect(self, amps):
        if self.load + amps > self.capacity:
            raise Exception('Connection will exceed capacity')
        elif self.load + amps < 0:
            raise Exception('Connection will cause negative load')
        else:
            self.load += amps

cb = CircuitBreaker(20)

print(cb.load)

cb.connect(12)
print('Add 12 amps: ', cb.load)

cb.connect(4)
print('Add 4 amps: ', cb.load)

print('Add 6 amps: ')
cb.connect(6)
