#!/usr/bin/env python3

f = 0
print(f)

f = "abc"
print(f)

print("string type " + str(123))

def myFunction():
    # global f
    f = "def"
    print(f)

myFunction()
print(f)

# del(f)
print(f)
