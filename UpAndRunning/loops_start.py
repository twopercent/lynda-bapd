#!/usr/bin/env python3

def main():
    x = 0
    while (x < 5):
        print(x)
        x += 1
    print("done\n")

def func1():
    x = 0
    for x in range(5, 10):
        print(x)
    print("done\n")

def func2():
    days = ["Mon", "Tue", "Wed", "Thu", "Fri"]
    for d in days:
        print(d)
    print("done\n")

def func3():
    for x in range(5, 10):
        if (x == 7): break
        print(x)
    print("done\n")

def func4():
    for x in range(5, 10):
        if (x % 2 == 0): continue
        print(x)
    print("done\n")

def func5():
    days = ["Mon", "Tue", "Wed", "Thu", "Fri"]
    for i, d in enumerate(days):
        print(i, d)
    print("done\n")
     

if __name__ == "__main__":
    main()
    func1()
    func2()
    func3()
    func4()
    func5()
