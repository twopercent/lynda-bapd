#!/usr/bin/env python3

import xml.dom.minidom
import os

def main():
    if os.path.exists("samplexml.xml") == False:
        os.system('wget ' + 'http://www.w3schools.com/xml/note.xml' + ' -O samplexml.xml')

    doc = xml.dom.minidom.parse("samplexml.xml")

    print(doc.nodeName)
    print(doc.firstChild.tagName)

    headings = doc.getElementsByTagName("heading")
    print("%d headings:" % headings.length)
    for heading in headings:
        print(heading.getAttribute("name"))
        print(heading)

    newHeading = doc.createElement("heading")
    newHeading.setAttribute("name", "North")
    doc.firstChild.appendChild(newHeading)
    print()

    headings = doc.getElementsByTagName("heading")
    print("%d headings:" % headings.length)
    for heading in headings:
        print(heading.getAttribute("name"))
        print(heading)

if __name__ == "__main__":
    main()


