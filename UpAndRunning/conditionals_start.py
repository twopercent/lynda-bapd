#!/usr/bin/env python3

def main():
    x, y = 100, 100

    if (x < y):
        st = "x is less than y"
    elif (x == y):
        st = "x is the same as y"
    else:
        st = "y is less that x"
    print(st)

# a if C else b
def alternate():
    x, y = 100, 100
    st = "x is less thatn y" if (x < y) else "x is greather than or equal to y"
    print(st)

if __name__ == "__main__":
    main()
    alternate()
