#!/usr/bin/env python3

import calendar

def main():
    c = calendar.TextCalendar(calendar.SUNDAY)
    str = c.formatmonth(2017, 2, 0, 0)

    #print(c)
    print(str)

    hc = calendar.HTMLCalendar(calendar.SUNDAY)
    str = hc.formatmonth(2017, 1)

    #print(str)

    for i in c.itermonthdays(2017, 2):
        #print(i)
        break

    for name in calendar.month_name:
        print(name)

    print()

    for day in calendar.day_name:
        print(day)

    print()

    for m in range(1,13):
        cal = calendar.monthcalendar(2017, m)

        weekone = cal[0]
        weektwo = cal[1]

        if weekone[calendar.FRIDAY] != 0:
            meetday = weekone[calendar.FRIDAY]
        else:
            meetday = weektwo[calendar.FRIDAY]

        print("Meetday is %10s %2d" % (calendar.month_name[m], meetday))

if __name__ == "__main__":
    main()
