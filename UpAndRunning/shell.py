#!/usr/bin/env python3

import os
import shutil
from os import path
from zipfile import ZipFile

def main():

    if path.exists("textfile.txt"):
        global src
        src = path.realpath("textfile.txt")

    head, tail = path.split(src)
    print("path: " + head)
    print("path: " + tail)

    dst = src + ".bak"
    shutil.copy(src, dst)

    shutil.copystat(src, dst)

def func1():
    os.rename("textfile.txt", "newfile.txt") 

def func2():
    root_dir, tail = path.split(src)
    shutil.make_archive("archive", "zip", root_dir)

def func3():
    with ZipFile("testzip.zip", "w") as newzip:
        newzip.write("textfile.txt")
        newzip.write("textfile.txt.bak")


if __name__ == "__main__":
    main()
    #func1()
    #func2()
    func3()
