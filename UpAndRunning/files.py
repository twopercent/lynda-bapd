#!/usr/bin/env python3

def main():
    #f = open("textfile.txt", "w+")
    f = open("textfile.txt", "a+")

    for i in range(10):
        f.write("This is line %d\r\n" % (i+1))

    f.close()

def func2():
    f = open("textfile.txt", "r")
    if f.mode == 'r':
        contents = f.read()
        print(contents)

    f.close()

def func3():
    f = open("textfile.txt", "r")
    fl = f.readlines()
    for x in fl:
        print(x)

    f.close()

if __name__ == "__main__":
    main()

func2()
func3()
