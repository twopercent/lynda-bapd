#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk

def main():
    root = Tk()
    button = ttk.Button(root, text = 'Click Me')
    button.pack()
    print('test')
    button['text']
    button['text'] = 'Press Me'
    button.config(text = 'Push Me')
    root.mainloop()

if __name__ == '__main__': main()
