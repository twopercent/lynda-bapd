#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk

class Feedback:

    def clearfunc(self):
        self.name.delete(0, END)
        self.email.delete(0, END)
        self.comments.delete(1.0, END)

    def submitfunc(self):
        print('Name: ' + self.name.get())
        print('Email: ' + self.email.get())
        print('Comments: ' + self.comments.get(1.0, END))

    def __init__(self, master):

        self.pic = PhotoImage(file = '/home/cmoser/projects/lynda-bapd/GUIDevelopment/desert_bug.gif')
        self.imagelabel = ttk.Label()
        self.imagelabel.pack()
        self.imagelabel.config(image = self.pic)

        self.instruc = ttk.Label(text = 'Fill out the form', font = 'bold')
        self.instruc.pack()

        self.namelabel = ttk.Label(text = 'Name:')
        self.namelabel.pack()
        self.name = ttk.Entry()
        self.name.pack()

        self.emaillabel = ttk.Label(text = 'Email:')
        self.emaillabel.pack()
        self.email = ttk.Entry()
        self.email.pack()

        self.commentlabel = ttk.Label(text = 'Comments:')
        self.commentlabel.pack()
        self.comments = Text(width = 40, height = 10)
        self.comments.pack()

        self.clearbutton = ttk.Button(text = 'Clear', command = self.clearfunc).pack(side = LEFT)
        self.submitbutton = ttk.Button(text = 'Submit', command = self.submitfunc).pack(side = RIGHT)

def main():

    root = Tk()
    feedback = Feedback(root)
    root.mainloop()

if __name__ == '__main__': main()
