#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk

root = Tk()

label = ttk.Label(root, text = 'Hello, Tkinter!')
label.pack()


label.config(text = 'Howdy, Tkinter! It\'s been a while since we last met.')

label.config(wraplength = 150)

root.mainloop()
