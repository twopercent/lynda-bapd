#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk

root = Tk()

def key_press(event):
    print('type: {}'.format(event.type))
    print('widget: {}'.format(event.widget))
    print('char: {}'.format(event.char))
    print('keysym: {}'.format(event.keysym))
    print('keycode: {}'.format(event.keycode))

def shortcut(action):
    print(action)

root.bind('<KeyPress>', key_press)

# lambda function with a bind method, since the bind is passing an event
# we neet to capture it here (with 'e')
root.bind('<Control-c>', lambda e: shortcut('Copy'))
root.bind('<Control-v>', lambda e: shortcut('Paste'))

root.mainloop()
